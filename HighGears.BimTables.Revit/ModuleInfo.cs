﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;

namespace HighGears.BimTables.Revit
{
    using Tools;

    internal class ModuleInfo
    {
        class Consts
        {
            public const string Id = "HighGears.BimTables.Revit";
            public const string PublicName = "Bim Tables";
            public const string SchemaVendorId = "PTREVIT";
            public const string TabName = "BIM Tables";
            public const string PaneId = "{41799D0D-8E66-4127-ADA7-E548FCBE9CC3}";
            public const string PaneTitle = "BIM Tables";
        }

        public ModuleInfo()
        {
        }

        public string ProductName { get { return string.Format("{0} ({1})", GetAddInName(), Build); } }
        public int Build { get { return GetBuildNumber(); } }

        public string GetAddInName() { return Consts.PublicName; }
        public string GetAddInId() { return Consts.Id; }
        public string GetSchemaVendorId() { return Consts.SchemaVendorId; }
        public string GetRibbonTabName() { return (Consts.TabName); }
        public string GetPaneId() { return (Consts.PaneId); }
        public string GetPaneTitle() { return (Consts.PaneTitle); }

        Assembly GetAssembly()
        {
            return typeof(RevitApp).Assembly;
        }

        public string GetVersion()
        {
            Version v = GetAssembly().GetName().Version;
            string version = string.Format("{0}.{1}.{2}", v.Major, v.Minor, v.Build);
            return (version);
        }

        public int GetBuildNumber()
        {
            return (GetAssembly().GetName().Version.Build);
        }

        public string GetLang() { return ("en"); }

        public string GetInstallPath(string relative)
        {
            return Path.Combine(Ops.GetHomeDir(), relative);
        }

        public string GetFamilyPath(string family)
        {
            string relative = string.Format("Content\\Families\\{0}.rfa", family);
            return GetInstallPath(relative);
        }

        public string GetUserDirectoryPath(string relative)
        {
            string basedir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            return Path.Combine(basedir, GetAddInName(), relative);
        }
    }
}
