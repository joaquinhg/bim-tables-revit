﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cad = Autodesk.Revit.DB;

namespace HighGears.BimTables.Revit.Services.Revit
{
    internal class RevitFailures
    {
        class Ids
        {
            //public static readonly Guid MemberDeleted = new Guid("{A44D4CAA-EDE0-40B1-A0C8-0E1F803481C6}");
            //public static readonly Guid MemberCopied = new Guid("{D3A06C01-E95F-468C-BA65-C8597F9EBF11}");
            //public static readonly Guid MemberModified = new Guid("{FB0863E1-9174-42CB-A0C5-4559E8731D4B}");
        }

        public static void RegisterAll()
        {
            //Cad.FailureDefinition.CreateFailureDefinition(
            //    new Cad.FailureDefinitionId(Ids.MemberDeleted),
            //    Cad.FailureSeverity.Error,
            //    "Cannot delete a tendon member");

            //Cad.FailureDefinition.CreateFailureDefinition(
            //    new Cad.FailureDefinitionId(Ids.MemberCopied),
            //    Cad.FailureSeverity.Error,
            //    "Cannot copy a tendon member");

            //Cad.FailureDefinition.CreateFailureDefinition(
            //    new Cad.FailureDefinitionId(Ids.MemberModified),
            //    Cad.FailureSeverity.Error,
            //    "Cannot modify this tendon member");
        }

        //public static Cad.FailureMessage MemberDeletedMessage(IEnumerable<Cad.ElementId> toshow)
        //{
        //    Cad.FailureMessage msg = new Cad.FailureMessage(new Cad.FailureDefinitionId(Ids.MemberDeleted));

        //    msg.SetFailingElements(toshow.ToList());

        //    return msg;
        //}

        //public static Cad.FailureMessage MemberCopiedMessage(IEnumerable<Cad.ElementId> toshow)
        //{
        //    Cad.FailureMessage msg = new Cad.FailureMessage(new Cad.FailureDefinitionId(Ids.MemberCopied));

        //    msg.SetFailingElements(toshow.ToList());

        //    return msg;
        //}

        //public static Cad.FailureMessage MemberModifiedMessage(IEnumerable<Cad.ElementId> toshow)
        //{
        //    Cad.FailureMessage msg = new Cad.FailureMessage(new Cad.FailureDefinitionId(Ids.MemberModified));

        //    msg.SetFailingElements(toshow.ToList());

        //    return msg;
        //}
    }
}
