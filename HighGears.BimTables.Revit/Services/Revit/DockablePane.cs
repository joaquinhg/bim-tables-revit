﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Win32;

using CadUi = Autodesk.Revit.UI;

namespace HighGears.BimTables.Revit.Services.Revit
{
    public class DockablePane<T>: CadUi.IDockablePaneProvider where T: FrameworkElement
    {
        CadUi.UIControlledApplication m_uicapp;
        CadUi.DockablePaneId m_pane_id;
        string m_title;
        T m_control;
        CadUi.DockablePaneId m_tab_behind { get; set; }

        public DockablePane(
            CadUi.UIControlledApplication _uicapp,
            Guid _id,
            string _title,
            T _control,
            CadUi.DockablePaneId _tab_behind)
        {
            m_uicapp = _uicapp;
            m_pane_id = new CadUi.DockablePaneId(_id);
            m_title = _title;
            m_control = _control;
            m_tab_behind = _tab_behind;
        }

        public T Control { get { return (m_control); } }

        public void SetupDockablePane(CadUi.DockablePaneProviderData _data)
        {
            CadUi.DockablePaneState state = new CadUi.DockablePaneState();
            state.DockPosition = CadUi.DockPosition.Tabbed;
            state.TabBehind = m_tab_behind;

            _data.FrameworkElement = m_control;
            _data.InitialState = state;
        }

        public void Register()
        {
            try
            {
                CadUi.DockablePane pane = m_uicapp.GetDockablePane(m_pane_id);
            }
            catch
            {
                m_uicapp.RegisterDockablePane(m_pane_id, m_title, this);
            }
        }

        public void Toggle()
        {
            if (m_control == null) return;

            CadUi.DockablePane pane = m_uicapp.GetDockablePane(m_pane_id);
            if (pane != null)
            {
                if (IsShown(pane))
                {
                    Hide(pane);
                }
                else
                {
                    Show(pane);
                }
            }
        }


        bool IsShown(CadUi.DockablePane _pane)
        {
#if REVIT2014
            return (GetLastVisibleState());
#else
            if (_pane == null) return(false);

            return(_pane.IsShown());
#endif
        }
        public bool IsShown()
        {
            CadUi.DockablePane pane = m_uicapp.GetDockablePane(m_pane_id);
            return(IsShown(pane));
        }

        void Show(CadUi.DockablePane _pane)
        {
            if (_pane != null)
            {
                _pane.Show();
                SetLastVisibleState(true);
            }
        }
        public void Show()
        {
            CadUi.DockablePane pane = m_uicapp.GetDockablePane(m_pane_id);
            Show(pane);
        }

        void Hide(CadUi.DockablePane _pane)
        {
            if (_pane != null)
            {
                _pane.Hide();
                SetLastVisibleState(false);
            }
        }
        public void Hide()
        {
            CadUi.DockablePane pane = m_uicapp.GetDockablePane(m_pane_id);
            Hide(pane);
        }

        string GetPersistentStateKey()
        {
            string key = @"HKEY_CURRENT_USER\Software\High Gears\HighGears.Framework.Revit\2014\PaneVisibleState";
            return(key);
        }

        bool GetLastVisibleState()
        {
#if REVIT2014
            bool r = true;
            object obj = Registry.GetValue(GetPersistentStateKey(), m_pane_id.Guid.ToString(), true);
            if (obj is string) bool.TryParse((string)obj, out r);
            return(r);
#else
            throw new NotImplementedException();
#endif
        }

        void SetLastVisibleState(bool _state)
        {
#if REVIT2014
            Registry.SetValue(GetPersistentStateKey(), m_pane_id.Guid.ToString(), _state);
#endif
        }
    }
}
