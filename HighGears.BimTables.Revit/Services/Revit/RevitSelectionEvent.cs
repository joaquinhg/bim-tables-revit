﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cad = Autodesk.Revit.DB;
using CadUi = Autodesk.Revit.UI;

namespace HighGears.BimTables.Revit.Services.Revit
{
    using UI;

    internal class RevitSelectionEvent
    {
        public RevitSelectionEvent(CadUi.UIControlledApplication uicapp)
        {
            UICApp = uicapp;
            Errors = new List<Exception>();
        }

        public CadUi.UIControlledApplication UICApp { get; private set; }
        List<Exception> Errors { get; set; }

        internal void Init()
        {
            UICApp.Idling += UICApp_Idling;
        }

        internal void Release()
        {
            UICApp.Idling -= UICApp_Idling;
        }

        void UICApp_Idling(object sender, CadUi.Events.IdlingEventArgs e)
        {
            try
            {
                CadUi.UIApplication uiapp = (CadUi.UIApplication)sender;

                if (uiapp.ActiveUIDocument == null) return;
                ICollection<Cad.ElementId> selected = uiapp.ActiveUIDocument.Selection.GetElementIds();
                if (selected.Any())
                {
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                Errors.Add(ex);

                // safeguard from locking the user out when error generated at each idle call
                if (Errors.Count < 5)
                {
                    Dialogs.Helper.Exception(ex);
                }
            }
        }
    }
}
