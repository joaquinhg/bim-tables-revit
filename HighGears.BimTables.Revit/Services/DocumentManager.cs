﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cad = Autodesk.Revit.DB;
using CadUi = Autodesk.Revit.UI;

namespace HighGears.BimTables.Revit.Services
{
    using Tools;

    public class DocumentManager
    {
        protected DocumentManager(Cad.Document doc)
        {
            Document = doc;
            UIDocument = new CadUi.UIDocument(Document);
            DocTools = new DocTools(doc);
        }

        public Cad.Document Document { get; private set; }
        public CadUi.UIDocument UIDocument { get; private set; }
        public DocTools DocTools { get; private set; }
    }
}
