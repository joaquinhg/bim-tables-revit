using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using Cad = Autodesk.Revit.DB;
using CadStorage = Autodesk.Revit.DB.ExtensibleStorage;
using CadApp = Autodesk.Revit.ApplicationServices;
using CadUi = Autodesk.Revit.UI;
using CadSelection = Autodesk.Revit.UI.Selection;
using CadException = Autodesk.Revit.Exceptions;
using CadAttribs = Autodesk.Revit.Attributes;

namespace HighGears.BimTables.Revit.Commands
{
    using Data;
    using Exceptions;
    using Tools;
    using Services;
    using Services.Revit;
    using UI;

    [CadAttribs.Regeneration(CadAttribs.RegenerationOption.Manual)]
    [CadAttribs.Transaction(CadAttribs.TransactionMode.Manual)]
    public class DevListData : CadUi.IExternalCommand
    {
        public DevListData()
        {
        }

        public CadUi.Result Execute(CadUi.ExternalCommandData _cmd, ref string _outmsg, Cad.ElementSet _eset)
        {
            CadUi.Result res = CadUi.Result.Failed;

            try
            {
                UIDocument = _cmd.Application.ActiveUIDocument;
                Cad.Document doc = UIDocument.Document;

                res = CadUi.Result.Succeeded;
            }
            catch (Exception ex)
            {
                Dialogs.Helper.Exception(ex);
            }
            return (res);
        }

        CadUi.UIDocument UIDocument { get; set; }
    }
}