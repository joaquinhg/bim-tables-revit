﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CadUi = Autodesk.Revit.UI;

namespace HighGears.BimTables.Revit.Commands.Availability
{
    public class BaseAvailabiilty
    {
        protected BaseAvailabiilty()
        {
        }

        public bool IsValid(CadUi.UIApplication applicationData)
        {
            return IsValidDocument(applicationData.ActiveUIDocument) && IsValidLicense() && IsEnabledIn(applicationData.ActiveUIDocument);
        }

        bool IsValidDocument(CadUi.UIDocument uidoc)
        {
            return uidoc != null && !uidoc.Document.IsFamilyDocument;
        }

        public bool IsValidLicense()
        {
            return true;
        }

        public bool IsEnabledIn(CadUi.UIDocument uidoc)
        {
            return true;
            //return RevitApp.Loaded.Controllers.Get(uidoc.Document).IsEnabled();
        }
    }
}
