﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cad = Autodesk.Revit.DB;
using CadUi = Autodesk.Revit.UI;

namespace HighGears.BimTables.Revit.Commands.Availability
{
    public class DocumentAvailability : BaseAvailabiilty, CadUi.IExternalCommandAvailability
    {
        public bool IsCommandAvailable(CadUi.UIApplication applicationData, Cad.CategorySet selectedCategories)
        {
            return base.IsValid(applicationData);
        }
    }
}
