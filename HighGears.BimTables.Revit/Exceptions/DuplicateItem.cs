﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighGears.BimTables.Revit.Exceptions
{
    public class DuplicateItem: MyException
    {
        public DuplicateItem(string id)
            : base("Duplicate item: {0}", id)
        {
        }
    }
}
