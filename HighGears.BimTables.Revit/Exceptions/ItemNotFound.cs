﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighGears.BimTables.Revit.Exceptions
{
    public class ItemNotFound: MyException
    {
        public ItemNotFound(string id)
            : base("Item not found: {0}", id)
        {
        }
    }
}
