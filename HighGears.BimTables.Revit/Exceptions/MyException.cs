﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighGears.BimTables.Revit.Exceptions
{
    public class MyException: Exception
    {
        protected MyException(string msg)
            : base(msg)
        {
        }

        protected MyException(string fmt, params object[] args)
            : base(string.Format(fmt, args))
        {
        }
    }
}
