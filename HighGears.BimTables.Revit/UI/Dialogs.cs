﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cad = Autodesk.Revit.DB;
using CadUi = Autodesk.Revit.UI;

namespace HighGears.BimTables.Revit.UI
{
    using Tools;

    internal class Dialogs
    {
        public Dialogs()
        {
            Info = new ModuleInfo();
        }

        ModuleInfo Info { get; set; }

        static Dialogs _helper;
        public static Dialogs Helper
        {
            get
            {
                if (_helper == null)
                {
                    _helper = new Dialogs();
                }
                return _helper;
            }
        }

        public void Error(string msg)
        {
            CadUi.TaskDialog.Show(Info.GetAddInName(), msg);
        }

        public bool Confirm(string msg)
        {
            CadUi.TaskDialogResult dr = CadUi.TaskDialog.Show("Confirm", msg, CadUi.TaskDialogCommonButtons.Yes | CadUi.TaskDialogCommonButtons.No);
            return dr == CadUi.TaskDialogResult.Yes;
        }

        public void Message(string msg, params object[] args)
        {
            string str = string.Format(msg, args);
            CadUi.TaskDialog.Show("Message", str);
        }

        public void TextBlock(string msg, params object[] args)
        {
            string str = string.Format(msg, args);
            new Views.TextBlockWindow("Message", str).ShowDialog();
        }

        internal void DebugInch(string prefix, Cad.XYZ v)
        {
            Message("{0}: ({1}, {2}, {3})", prefix, UnitsTool.FeetToInches(v.X), UnitsTool.FeetToInches(v.Y), UnitsTool.FeetToInches(v.Z));
        }
        internal void Debug(string prefix, Cad.XYZ v)
        {
            Message("{0}: ({1}, {2}, {3})", prefix, v.X, v.Y, v.Z);
        }

        public void Exception(Exception ex)
        {
            StringBuilder str = new StringBuilder();
            str.AppendLine(ex.Message);

#if DEBUG
            //str.AppendLine(ex.StackTrace);
#endif

            Error(str.ToString());
        }
    }
}
