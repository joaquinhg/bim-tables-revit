﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HighGears.BimTables.Revit.UI.Views
{
    /// <summary>
    /// Interaction logic for TextBlockWindow.xaml
    /// </summary>
    public partial class TextBlockWindow : Window
    {
        public TextBlockWindow(string title, string msg)
        {
            InitializeComponent();

            Title = title;
            Message = msg;
            DataContext = this;
        }

        public string Message { get; set; }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
