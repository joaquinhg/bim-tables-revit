﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HighGears.BimTables.Revit.UI.Views
{
    using ViewModels;

    /// <summary>
    /// Interaction logic for HeadingLabel.xaml
    /// </summary>
    public partial class HeadingLabel : UserControl
    {
        public HeadingLabel()
        {
            InitializeComponent();

            this.DataContext = this;
        }

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
                "Text",
                typeof(string),
                typeof(HeadingLabel),
                new PropertyMetadata(onTextChanged)
            );

        private static void onTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            HeadingLabel control = d as HeadingLabel;
            if (control != null)
            {
                string current = control.label_ctr.Content as string;
                if (current == null || !current.Equals(control.Text))
                {
                    control.label_ctr.Content = control.Text;
                }
            }
        }

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
    }
}
