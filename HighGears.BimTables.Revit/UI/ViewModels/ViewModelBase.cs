﻿namespace HighGears.BimTables.Revit.UI.ViewModels
{
    using Data;

    public abstract class ViewModelBase: ObservableData
    {
        protected ViewModelBase() { }

        public virtual void ValidateInputs() { }
    }
}
