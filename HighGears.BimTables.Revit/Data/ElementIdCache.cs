﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cad = Autodesk.Revit.DB;

namespace HighGears.BimTables.Revit.Data
{
    public class ElementIdCache<T>: Cache<T> where T : Cad.Element
    {
        public ElementIdCache() : base()
        {
        }

        protected override string GetId(T obj)
        {
            return IdStr.From(obj);
        }

        string StrKey(Cad.ElementId id)
        {
            return IdStr.From(id);
        }

        public bool Has(Cad.ElementId id)
        {
            return Has(StrKey(id));
        }

        public bool HasAny(IEnumerable<Cad.ElementId> ids)
        {
            foreach(Cad.ElementId id in ids)
            {
                if (Has(id)) return true;
            }
            return false;
        }

        public T Get(Cad.ElementId id)
        {
            return Get(StrKey(id));
        }
    }
}
