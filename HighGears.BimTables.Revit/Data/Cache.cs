﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighGears.BimTables.Revit.Data
{
    using Exceptions;

    public abstract class Cache<T>: IEnumerable<T>
    {
        Dictionary<string, T> _map;

        public Cache()
        {
            _map = new Dictionary<string, T>();
        }

        public IEnumerable<string> Ids { get { return _map.Keys; } }
        public IEnumerable<T> Items { get { return _map.Values; } }
        public int Count { get { return _map.Count; } }

        protected virtual string GetId(T obj) { return obj.GetHashCode().ToString(); }

        public void Clear() { _map.Clear(); }

        public void Add(T obj)
        {

            string id = GetId(obj);
            if (Has(id)) throw new DuplicateItem(id);
            _map[id] = obj;
        }

        public void AddIfMissing(T obj)
        {
            if (!Has(obj))
            {
                Add(obj);
            }
        }
        public void AddIfMissing(IEnumerable<T> objs)
        {
            foreach (T o in objs)
            {
                AddIfMissing(o);
            }
        }

        public void Add(IEnumerable<T> objs)
        {
            foreach (T o in objs)
            {
                Add(o);
            }
        }

        public void Set(T obj)
        {
            string id = GetId(obj);
            if (!Has(id)) throw new ItemNotFound(id);
            _map[id] = obj;
        }

        void Remove(string id)
        {
            if (Has(id))
            {
                _map.Remove(id);
            }
        }

        public void Remove(T obj)
        {
            string id = GetId(obj);
            Remove(id);
        }
        public void Remove(IEnumerable<string> ids)
        {
            foreach (string id in ids)
            {
                Remove(id);
            }
        }

        public T Get(string id)
        {
            T obj = default(T);
            if (_map.ContainsKey(id))
            {
                obj = _map[id];
            }

            return obj;
        }
        public bool Has(string id)
        {
            return _map.ContainsKey(id);
        }
        public bool Has(T obj)
        {
            return Has(GetId(obj));
        }

        public void Invalidate() { _map.Clear(); }

        public IEnumerator<T> GetEnumerator()
        {
            return _map.Values.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _map.Values.GetEnumerator();
        }
    }
}
