﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cad = Autodesk.Revit.DB;

namespace HighGears.BimTables.Revit.Data
{
    internal class IdStr
    {
        public static string From(Cad.ElementId id) { return id.IntegerValue.ToString(); }
        public static string From(Cad.Element element) { return From(element.Id); }
        public static Cad.ElementId To(string id)
        {
            if (int.TryParse(id, out int val)) return new Cad.ElementId(val);

            return Cad.ElementId.InvalidElementId;
        }
    }
}
