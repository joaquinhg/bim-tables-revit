﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace HighGears.BimTables.Revit.Data
{
    public abstract class ObservableData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected ObservableData() { }
    }
}
