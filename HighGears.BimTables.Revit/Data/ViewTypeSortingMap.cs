﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cad = Autodesk.Revit.DB;

namespace HighGears.BimTables.Revit.Data
{
    public class ViewTypeSortingMap: SortingMap<Cad.ViewType>
    {
        public static int IndexFor(Cad.View view)
        {
            return DefaultMap.Indexer(view);
        }

        static readonly ViewTypeSortingMap DefaultMap = Default();

        static ViewTypeSortingMap Default()
        {
            ViewTypeSortingMap map = new ViewTypeSortingMap();
            map[Cad.ViewType.FloorPlan] = 1;
            map[Cad.ViewType.EngineeringPlan] = 2;
            map[Cad.ViewType.CeilingPlan] = 3;
            map[Cad.ViewType.AreaPlan] = 4;

            map[Cad.ViewType.ThreeD] = 100;

            map[Cad.ViewType.Elevation] = 200;

            map[Cad.ViewType.Section] = 300;

            map[Cad.ViewType.Detail] = 400;

            map[Cad.ViewType.DraftingView] = 500;

            map[Cad.ViewType.Legend] = 600;

            map[Cad.ViewType.Schedule] = 700;
            map[Cad.ViewType.ColumnSchedule] = 701;
            map[Cad.ViewType.PanelSchedule] = 702;

            map[Cad.ViewType.DrawingSheet] = 800;

            return map;
        }

        public int Indexer(Cad.View view)
        {
            if (this.ContainsKey(view.ViewType)) return this[view.ViewType];
            return int.MaxValue;
        }
    }
}
