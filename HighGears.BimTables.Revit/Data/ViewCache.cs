﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cad = Autodesk.Revit.DB;

namespace HighGears.BimTables.Revit.Data
{
    public class ViewCache : ElementIdCache<Cad.View>
    {
        public void Remove(Cad.ElementId id)
        {
            base.Remove(new string[] { IdStr.From(id) });
        }
    }
}
