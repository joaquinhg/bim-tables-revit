﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

using Cad = Autodesk.Revit.DB;
using CadApp = Autodesk.Revit.ApplicationServices;
using CadUi = Autodesk.Revit.UI;
using CadAttribs = Autodesk.Revit.Attributes;

namespace HighGears.BimTables.Revit
{
    using Data;
    using Services;
    using Services.Controllers;
    using Services.Revit;
    using Tools;
    using UI.Views;
    using UI.ViewModels;

    public class RevitApp : CadUi.IExternalApplication
    {
        class Const
        {
        }

        CadUi.UIControlledApplication m_uicapp;
        Logger m_log;
        Cad.AddInId m_addin_id;
        internal ModuleInfo Info { get; private set; }
        public DocumentControllersManager Controllers { get; private set; }
        RevitSelectionEvent SelectionEvent { get; set; }

        public RevitApp()
            : this(Ops.GetHomeDir())
	    {
        }
        public RevitApp(string homedir)
        {
            s_singleton = this;
            Ops.SetHomeDir(homedir);

            Info = new ModuleInfo();
            Controllers = new DocumentControllersManager(Info.GetInstallPath(string.Empty), Log);
        }

        static RevitApp s_singleton;
        public static RevitApp Loaded
        {
            get
            {
                if (s_singleton == null)
                {
                    s_singleton = new RevitApp(Ops.GetHomeDir());
                }
                return (s_singleton);
            }
        }

        public Guid AppId { get { return m_addin_id.GetGUID(); } }

        public Logger Log
        {
            get
            {
                if (m_log == null)
                {
                    string fpath = Info.GetInstallPath(string.Format("{0}.txt", new ModuleInfo().GetAddInId()));
                    File.AppendAllText(fpath, string.Format("> **SESSION START: {0}\n", DateTime.Now));

                    m_log = new Logger(Info.GetAddInId(), fpath);
                }
                return m_log;
            }
        }

        Stats _stats;
        public Stats Stats
        {
            get
            {
                if (_stats == null)
                {
                    _stats = new Stats(Log);
                }
                return _stats;
            }
        }

        public CadUi.Result OnStartup(CadUi.UIControlledApplication _uicapp)
        {
            try
            {
                m_uicapp = _uicapp;
                m_addin_id = _uicapp.ActiveAddInId;

                //UI.Dialogs.Helper.Message("loading");
                
                InitRevitItems();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

            return(CadUi.Result.Succeeded);
        }

        public CadUi.Result OnShutdown(CadUi.UIControlledApplication _uicapp)
        {
            try
            {
                RemoveEvents();
                UnregisterUpdaters();
                RevitApp.Loaded.Stats.PrintAverages();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

            return (CadUi.Result.Succeeded);
        }


        void InitRevitItems()
        {
            AddEvents();
            RegisterRevitFailures();
            RegisterUpdaters();
            AddRibbon();
            InitPane();
        }

        void AddEvents()
        {
            //SelectionEvent = new RevitSelectionEvent(m_uicapp);
            //SelectionEvent.Init();

            //m_uicapp.ControlledApplication.DocumentOpened += ControlledApplication_DocumentOpened;
            //m_uicapp.ControlledApplication.DocumentChanged += ControlledApplication_DocumentChanged;
            //m_uicapp.ControlledApplication.DocumentClosing += ControlledApplication_DocumentClosing;
        }

        void RemoveEvents()
        {
            if (SelectionEvent != null) SelectionEvent.Release();
        }

        void RegisterRevitFailures()
        {
            RevitFailures.RegisterAll();
        }

        void RegisterUpdaters()
        {
            //GripUpdater gripUpdater = new GripUpdater(m_uicapp.ActiveAddInId);

            //gripUpdater.RegisterSelf();
        }

        void UnregisterUpdaters()
        {
            //GripUpdater gripUpdater = new GripUpdater(m_uicapp.ActiveAddInId);

            //gripUpdater.UnregisterSelf();
        }

        void AddRibbon()
        {
            RibbonManager ribbon = new RibbonManager(m_uicapp, Info.GetRibbonTabName(), Ops.CurrentAssemblyPath());
            ribbon.Create();
        }

        void InitPane()
        {
        }

        bool IsManagedDocument(Cad.Document document)
        {
            return document != null;
        }

        void ControlledApplication_DocumentOpened(object sender, Cad.Events.DocumentOpenedEventArgs e)
        {
            try
            {
                if (IsManagedDocument(e.Document))
                {
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private void ControlledApplication_DocumentClosing(object sender, Cad.Events.DocumentClosingEventArgs e)
        {
            try
            {
                Cad.Document doc = e.Document;

                if (IsManagedDocument(e.Document))
                {
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        void ControlledApplication_DocumentChanged(object sender, Cad.Events.DocumentChangedEventArgs e)
        {
            try
            {
                Cad.Document doc = e.GetDocument();
                if (IsManagedDocument(doc))
                {
                    Cad.ElementFilter filter = new Cad.ElementClassFilter(typeof(Cad.View));

                    switch (e.Operation)
                    {
                        case Cad.Events.UndoOperation.TransactionCommitted:
                        case Cad.Events.UndoOperation.TransactionUndone:
                        case Cad.Events.UndoOperation.TransactionRedone:
                        case Cad.Events.UndoOperation.TransactionRolledBack:
                            ICollection<Cad.ElementId> added = e.GetAddedElementIds(filter);
                            ICollection<Cad.ElementId> deleted = e.GetDeletedElementIds();
                            if (added.Count > 0 || deleted.Count > 0)
                            {
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }
    }
}
