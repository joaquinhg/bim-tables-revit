﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.Windows.Media.Imaging;

using CadUi = Autodesk.Revit.UI;

namespace HighGears.BimTables.Revit
{
    using Commands;
    using Commands.Availability;
    using Tools;

    internal class RibbonManager
    {
        public RibbonManager(CadUi.UIControlledApplication uicapp, string tabName, string dllName)
        {
            UICApp = uicapp;
            TabName = tabName;
            DllName = dllName;
            Resources = new ResourceTool();
        }

        class Consts
        {
            public const string CustomizationPanel = "Customization";
            public const string ModelPanel = "Model";
            public const string TendonToolsPanel = "Tendon";
            public const string DatumPanel = "Datum";
            public const string VisibilityPanel = "Visibility";
            public const string SlabsPanel = "Slabs";
            public const string WarningsPanel = "Warnings";
            public const string DevPanel = "Dev";
        }

        public CadUi.UIControlledApplication UICApp { get; private set; }
        public string TabName { get; private set; }
        string DllName { get; set; }
        ResourceTool Resources { get; set; }

        public void Create()
        {
            //CreateCustomizationPanel();
            //CreateModelPanel();
            //CreateTendonToolsPanel();
            //CreateDatumPanel();
            //CreateVisibilityPanel();
            //CreateSlabsPanel();
            //CreateWarningsPanel();
#if DEBUG
            CreateDevPanel();
#endif
        }

        //        public void CreateCustomizationPanel()
        //        {
        //            CadUi.RibbonPanel panel = GetRibbonPanel(Consts.CustomizationPanel);

        //            CadUi.PushButtonData cmdProjectSettings = PushButton<ProjectSettingsCommand, DocumentAvailability>("Settings", ImgPath("Settings32.PNG"));
        //            CadUi.PushButtonData cmdRenderingSettings = PushButton<RenderingSettingsCommand, DocumentAvailability>("Rendering", ImgPath("SettingsBlue32.PNG"));
        //            //CadUi.PushButtonData cmdAbout = PushButton<About, AnyAvailability>("About", ImgPath("About32.PNG"));

        //            panel.AddItem(cmdProjectSettings);
        //            panel.AddItem(cmdRenderingSettings);
        //            //panel.AddItem(cmdAbout);
        //        }

        //        public void CreateModelPanel()
        //        {
        //            CadUi.RibbonPanel panel = GetRibbonPanel(Consts.ModelPanel);

        //            //CadUi.PushButtonData cmdImportPtb = PushButton<ImportPtb, DocumentAvailability>("Import", ImgPath("import32.PNG"));
        //            CadUi.PushButtonData cmdImportDxf = PushButton<ImportDxf, DocumentAvailability>("Import", ImgPath("import32.PNG"));
        //            CadUi.PushButtonData cmdMaterialTakeoff = PushButton<MaterialTakeoff, DocumentAvailability>("Material\nTakeoff", ImgPath("export32.PNG"));
        //            CadUi.PushButtonData cmdCreateFromPointSelection = PushButton<CreateFromPointSelection, DocumentAvailability>("Create", ImgPath("Create16.PNG"));
        //            CadUi.PushButtonData cmdCopySystem = PushButton<CopyFromSelection, PTSystemAvailability>("Copy", ImgPath("Copy16.PNG"));
        //            CadUi.PushButtonData cmdDeleteSystem = PushButton<DeleteSystem, PTSystemAvailability>("Delete", ImgPath("Delete32.PNG"));
        //            CadUi.PushButtonData cmdCreateAllLinks = PushButton<CreateAllLinks, DocumentAvailability>("Link All", ImgPath("link32.PNG"));
        //            CadUi.PushButtonData cmdUnlinkSelected = PushButton<UnlinkSelected, PTSystemAvailability>("Unlink Selected", ImgPath("Unlink32.PNG"));
        //            CadUi.PushButtonData cmdCreate3d = PushButton<Create3d, PTSystemAvailability>("Create 3D", ImgPath("Create 3D16.PNG"));

        //            //panel.AddItem(cmdImportPtb);
        //            panel.AddItem(cmdImportDxf);
        //            panel.AddItem(cmdMaterialTakeoff);
        //            panel.AddStackedItems(cmdCreateFromPointSelection, cmdCopySystem, cmdCreate3d);

        //            panel.AddItem(cmdDeleteSystem);
        //            panel.AddSeparator();

        //            panel.AddItem(cmdCreateAllLinks);
        //            panel.AddItem(cmdUnlinkSelected);
        //        }

        //        public void CreateTendonToolsPanel()
        //        {
        //            CadUi.RibbonPanel panel = GetRibbonPanel(Consts.TendonToolsPanel);

        //            CadUi.PushButtonData cmdSelectSystem = PushButton<SelectSystem, PTSystemAvailability>("Select System", ImgPath("Select16.PNG"));
        //            CadUi.PushButtonData cmdSelectParallel = PushButton<SelectParallel, PTSystemAvailability>("Select Parallel", ImgPath(""));
        //            CadUi.PushButtonData cmdSelectSlabs = PushButton<SelectSlabs, PTSystemAvailability>("Select Slabs", ImgPath(""));
        //            CadUi.PushButtonData cmdChangeEdgeCondition = PushButton<ChangeEdgeCondition, EdgeMemberAvailability>("Edge Condition", ImgPath(""));
        //            CadUi.PushButtonData cmdSwapEdgeConditions = PushButton<SwapEdgeConditions, PTSystemAvailability>("Swap Edge", ImgPath("swap16.PNG"));
        //            CadUi.PushButtonData cmdCopyEnds = PushButton<CopyEndMembers, PTSystemAvailability>("Copy Ends", ImgPath("Copy16.PNG"));
        //            CadUi.PushButtonData cmdAddTerminations = PushButton<AddTerminations, PTSystemAvailability>("Terminate Strands", ImgPath(""));
        //            CadUi.PushButtonData cmdRemoveTerminations = PushButton<RemoveTerminations, PTSystemAvailability>("Remove Termination", ImgPath("Delete16.PNG"));
        //            CadUi.PushButtonData cmdRegenLabels = PushButton<RegenLabels, DocumentAvailability>("Regenerate Labels", ImgPath("regen16.PNG"));
        //            CadUi.PushButtonData cmdUpdateLabelValues = PushButton<UpdateLabelValues, DocumentAvailability>("Update Labels", ImgPath("update16.PNG"));
        //            CadUi.PushButtonData cmdRenumber = PushButton<Renumber, DocumentAvailability>("Renumber", ImgPath("renumber16.PNG"));
        //            CadUi.PushButtonData cmdTextClashes = PushButton<TextClashes, DocumentAvailability>("Text Clashes", ImgPath("text clash16.PNG"));
        //            CadUi.PushButtonData cmdTendonClashes = PushButton<TendonClashes, DocumentAvailability>("Tendon Clashes", ImgPath("tendon clash16.PNG"));
        //            CadUi.PushButtonData cmdAlignBubble = PushButton<AlignBubble, PTSystemAvailability>("Align Bubble", ImgPath("align16.PNG"));
        //            CadUi.PushButtonData cmdTrimTendons = PushButton<TrimTendons, PTSystemAvailability>("Trim Tendons", ImgPath("align16.PNG"));
        //            CadUi.PushButtonData cmdAddDimension = PushButton<AddDimension, PTSystemAvailability>("Add Dimension", ImgPath("dimension16.PNG"));
        //            CadUi.PushButtonData cmdAlignToGrid = PushButton<AlignTendonsToGrid, DocumentAvailability>("Align Tendons", ImgPath("Align tendon16.PNG"));

        //            panel.AddStackedItems(cmdSelectSystem, cmdSelectParallel, cmdSelectSlabs);
        //            panel.AddSeparator();

        //            panel.AddStackedItems(cmdChangeEdgeCondition, cmdSwapEdgeConditions, cmdCopyEnds);
        //            panel.AddStackedItems(cmdAddTerminations, cmdRemoveTerminations);
        //            panel.AddSeparator();

        //            panel.AddStackedItems(cmdRegenLabels, cmdUpdateLabelValues, cmdRenumber);
        //            panel.AddSeparator();

        //            panel.AddStackedItems(cmdTextClashes, cmdTendonClashes);

        //            panel.AddSeparator();
        //            panel.AddStackedItems(cmdAlignBubble, cmdTrimTendons);
        //            panel.AddStackedItems(cmdAddDimension, cmdAlignToGrid);
        //        }

        //        public void CreateDatumPanel()
        //        {
        //            CadUi.RibbonPanel panel = GetRibbonPanel(Consts.DatumPanel);

        //            CadUi.PushButtonData cmdAddDatum = PushButton<AddDatum, DocumentAvailability>("Add", ImgPath(""));
        //            CadUi.PushButtonData cmdRelocateDatum = PushButton<RelocateDatum, DocumentAvailability>("Relocate", ImgPath(""));
        //            CadUi.PushButtonData cmdFindDatum = PushButton<FindDatum, DocumentAvailability>("Find", ImgPath(""));

        //            panel.AddStackedItems(cmdAddDatum, cmdRelocateDatum, cmdFindDatum);
        //        }

        //        public void CreateVisibilityPanel()
        //        {
        //            CadUi.RibbonPanel panel = GetRibbonPanel(Consts.VisibilityPanel);

        //            CadUi.SplitButtonData splitButtonData = new CadUi.SplitButtonData("chair-visibility", "Chairs");
        //            CadUi.SplitButton chairsButton = panel.AddItem(splitButtonData) as CadUi.SplitButton;

        //            CadUi.PushButtonData cmdShowAll = PushButton<ShowAll, DocumentAvailability>("Show All", ImgPath("show32.PNG"));
        //            CadUi.PushButtonData cmdHideChairLabels = PushButton<HideChairLabels, DocumentAvailability>("Hide Chair Labels", ImgPath("Hide32.PNG"));
        //            CadUi.PushButtonData cmdUnhideChairLabels = PushButton<UnhideChairLabels, DocumentAvailability>("Chair Labels", ImgPath("show32.PNG"));
        //            CadUi.PushButtonData cmdLatitudeTendons = PushButton<ShowLatitudeTendons, DocumentAvailability>("X Tendons", ImgPath("show32.PNG"));
        //            CadUi.PushButtonData cmdLongitudeTendons = PushButton<ShowLongitudeTendons, DocumentAvailability>("Y Tendons", ImgPath("show32.PNG"));

        //            chairsButton.AddPushButton(cmdShowAll);
        //            chairsButton.AddPushButton(cmdHideChairLabels);
        //            chairsButton.AddPushButton(cmdUnhideChairLabels);
        //            chairsButton.AddPushButton(cmdLatitudeTendons);
        //            chairsButton.AddPushButton(cmdLongitudeTendons);
        //        }

        //        public void CreateSlabsPanel()
        //        {
        //            //CadUi.RibbonPanel panel = GetRibbonPanel(Consts.SlabsPanel);

        //            CadUi.PushButtonData cmdSelectSlabs = PushButton<SelectSlabs, PTSystemAvailability>("Show", ImgPath(""));
        //            CadUi.PushButtonData cmdAttachSlabs = PushButton<AttachSlabs, PTSystemAvailability>("Attach", ImgPath(""));
        //            CadUi.PushButtonData cmdDetachSlabs = PushButton<DetachSlabs, PTSystemAvailability>("Detach", ImgPath(""));

        //            //panel.AddItem(cmdSelectSlabs);

        //            //panel.AddItem(cmdAttachSlabs);
        //            //panel.AddItem(cmdDetachSlabs);
        //        }

        //        public void CreateWarningsPanel()
        //        {
        //            CadUi.RibbonPanel panel = GetRibbonPanel(Consts.WarningsPanel);

        //            CadUi.PushButtonData cmdShowWarnings = PushButton<ShowWarnings, DocumentAvailability>("Show", ImgPath("show16.PNG"));
        //            CadUi.PushButtonData cmdClearAllWarnings = PushButton<ClearAllWarnings, DocumentAvailability>("Clear All", ImgPath(""));

        //            panel.AddStackedItems(cmdShowWarnings, cmdClearAllWarnings);
        //        }

        public void CreateDevPanel()
        {
            CadUi.RibbonPanel panel = GetRibbonPanel(Consts.DevPanel);

            CadUi.PushButtonData cmdListData = PushButton<DevListData, DocumentAvailability>("List Data", string.Empty);

            panel.AddItem(cmdListData);
        }

        CadUi.PushButtonData PushButton<T, A>(string name, string imgResource)
        {
            Type t = typeof(T);
            Type a = typeof(A);
            CadUi.PushButtonData data = new CadUi.PushButtonData(t.Name, name, DllName, t.FullName);
            data.AvailabilityClassName = a.FullName;
            if (!string.IsNullOrEmpty(imgResource))
            {
                data.Image = Resources.GetImage(imgResource);
                data.LargeImage = data.Image;
            }
            return data;
        }

        //        CadUi.SplitButtonData SplitButton<T>(string name)
        //        {
        //            Type t = typeof(T);
        //            CadUi.SplitButtonData data = new CadUi.SplitButtonData(t.Name, name);
        //            return data;
        //        }

        //        BitmapImage GetImage(string resource)
        //        {
        //            Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream(resource);
        //            if (s == null) return null;

        //            BitmapImage img = new BitmapImage();
        //            img.BeginInit();
        //            img.StreamSource = s;
        //            img.EndInit();
        //            return img;
        //        }

        //        string ImgPath(string name)
        //        {
        //            return string.Format("{0}.Resources.{1}", ResourcePrefix, name);
        //        }

        /// <summary>
        /// Returns a RibbonPanel named _name
        /// </summary>
        /// <param name="_name"></param>
        /// <returns></returns>
        CadUi.RibbonPanel GetRibbonPanel(string _name)
        {
            InitTab();
            CadUi.RibbonPanel panel = FindRibbonPanel(_name);
            if (panel == null)
            {
                panel = UICApp.CreateRibbonPanel(TabName, _name);
            }
            return (panel);
        }

        void InitTab()
        {
            if (!IsDefaultTabCreated())
            {
                CreateDefaultTab();
            }
        }

        /// <summary>
        /// Returns true if the default tab has already been created
        /// </summary>
        /// <returns></returns>
        bool IsDefaultTabCreated()
        {
            bool r = false;
            try
            {
                UICApp.GetRibbonPanels(TabName);
                r = true;
            }
            catch
            {
            }
            return (r);
        }

        void CreateDefaultTab()
        {
            UICApp.CreateRibbonTab(TabName);
        }

        CadUi.RibbonPanel FindRibbonPanel(string _name)
        {
            var q = from panel in UICApp.GetRibbonPanels(TabName)
                    where panel.Name == _name
                    select panel;

            return (q.FirstOrDefault());
        }
    }
}
