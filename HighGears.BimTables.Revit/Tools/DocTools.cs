﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cad = Autodesk.Revit.DB;

namespace HighGears.BimTables.Revit.Tools
{
    //using Data;

    public class DocTools
    {
        public DocTools(Cad.Document doc)
        {
            Document = doc;
        }

        public Cad.Document Document { get; private set; }

        //public Cad.FamilySymbol Find(FamilySymbolReference symref)
        //{
        //    Cad.Family family = GetFamily(symref.FamilyName);
        //    if (family == null) return null;

        //    var q = GetElements<Cad.FamilySymbol>(family.GetFamilySymbolIds());
        //    Cad.FamilySymbol sym = q.Where(fsym => symref.TypeName.Equals(fsym.Name, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
        //    if (sym != null && !sym.IsActive && !sym.Document.IsReadOnly) sym.Activate();

        //    return sym;
        //}

        //public FamilySymbolReference GetFamilySymbolReference(Cad.FamilySymbol sym)
        //{
        //    return new FamilySymbolReference(sym.Family.Name, sym.Name);
        //}

        public Cad.Family GetFamily(string _family) { return GetElementType<Cad.Family>(_family); }

        public T GetElementType<T>(string _name) where T : Cad.Element
        {
            Cad.FilteredElementCollector collector = new Cad.FilteredElementCollector(Document);
            foreach (Cad.Element e in collector.OfClass(typeof(T)))
            {
                if (e is T && e.Name.Equals(_name, StringComparison.OrdinalIgnoreCase)) return ((T)e);
            }
            return (default(T));
        }

        public IEnumerable<T> GetElements<T>(IEnumerable<Cad.ElementId> _ids) where T : Cad.Element
        {
            foreach (Cad.ElementId id in _ids)
            {
                yield return ((T)Document.GetElement(id));
            }
        }

        public IEnumerable<T> GetElements<T>(IEnumerable<string> _ids) where T : Cad.Element
        {
            foreach (string id in _ids)
            {
                T elem = ((T)Document.GetElement(id));
                if (elem == null) continue;
                yield return elem;
            }
        }

        public List<T> GetElementTypes<T>() where T : Cad.ElementType
        {
            Cad.FilteredElementCollector collector = new Cad.FilteredElementCollector(Document);
            Cad.ElementFilter class_f = new Cad.ElementClassFilter(typeof(T));

            List<T> found = new List<T>();
            foreach (Cad.Element elem in collector.WherePasses(class_f))
            {
                if (elem is T) found.Add((T)elem);
            }
            return (found);
        }

        public List<T> GetElementTypes<T>(Cad.BuiltInCategory catId) where T : Cad.ElementType
        {
            Cad.FilteredElementCollector collector = new Cad.FilteredElementCollector(Document);
            Cad.ElementFilter class_f = new Cad.ElementClassFilter(typeof(T));
            Cad.ElementFilter cat_f = new Cad.ElementCategoryFilter(catId);
            Cad.ElementFilter and_f = new Cad.LogicalAndFilter(class_f, cat_f);

            List<T> found = new List<T>();
            foreach (Cad.Element elem in collector.WherePasses(and_f))
            {
                if (elem is T) found.Add((T)elem);
            }
            return (found);
        }
    }
}
