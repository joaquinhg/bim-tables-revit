using System;
using System.Collections.Generic;

namespace HighGears.BimTables.Revit.Services
{
    using Tools;

    public class counter
    {
	    public DateTime m_start;
	    public DateTime m_end;
	    public Dictionary<string, int> m_keys;
        public TimeSpan m_total;
        public int m_count;
	
	    public counter()
	    {
		    m_keys = new Dictionary<string,int>();
            clear_totals();
	    }

	    public void clear_totals()
        {
            m_total = TimeSpan.Zero;
            m_count = 0;
        }

        public void reset()
	    {
		    foreach (string key in m_keys.Keys) {
			    m_keys[key] = 0;
		    }
	    }

	    public DateTime start()
	    {
		    reset();
		    m_start = DateTime.Now;
		    return(m_start);
	    }

	    /// <summary>
	    /// Returns the interval from when start() was called
	    /// </summary>
	    /// <param name="_name"></param>
	    /// <returns></returns>
	    public TimeSpan stop()
	    {
		    m_end = DateTime.Now;
		    TimeSpan interval = m_end - m_start;
            m_total += interval;
            m_count++;
		    return(interval);
	    }

        /// <summary>
        /// returns the average time of each counter start()/stop() intervals
        /// </summary>
        /// <returns></returns>
        public TimeSpan average()
        {
            TimeSpan ave = TimeSpan.Zero;
            if (m_count != 0) ave = new TimeSpan(m_total.Ticks / m_count);
            return(ave);
        }

	    public int increment(string _key)
	    {
		    if (!m_keys.ContainsKey(_key)) m_keys[_key] = 0;
		    else m_keys[_key]++;

		    return(m_keys[_key]);
	    }

	    public int get(string _key)
	    {
		    return(m_keys[_key]);
	    }
    }

    public class Stats
    {
	    Dictionary<string, counter> m_data;
        const string MAIN = "unnamed";

	    public Stats(Logger logger)
	    {
		    m_data = new Dictionary<string,counter>();
            Logger = logger;
	    }

        Logger Logger { get; set; }

	    string safe_name(string _name)
	    {
		    if (_name == null) return(MAIN);
		    return(_name);
	    }

	    public DateTime Start(string _name)
	    {
		    _name = safe_name(_name);
		    if (!m_data.ContainsKey(_name)) {
			    m_data[_name] = new counter();
		    }
		    return(m_data[_name].start());
	    }

	    public TimeSpan Stop(string _name)
	    {
		    _name = safe_name(_name);
		    TimeSpan proc_time = m_data[_name].stop();
		    if (Logger != null) {
			    Logger.Debug("> STAT [{4}]: {0,2:00}:{1,2:00}:{2,2:00}:{3,2:000}",
				    proc_time.Hours, proc_time.Minutes, proc_time.Seconds, proc_time.Milliseconds,
				    _name
			    );
		    }
		    return(proc_time);
	    }

        public void StartDebug(string _name)
        {
#if DEBUG
            Start(_name);
#endif
        }

        public void StopDebug(string _name)
        {
#if DEBUG
            Stop(_name);
#endif
        }

	    public int Increment(string _name, string _key)
	    {
		    _name = safe_name(_name);
		    return(m_data[_name].increment(_key));
	    }

	    public int Get(string _name, string _key)
	    {
		    _name = safe_name(_name);
		    return(m_data[_name].m_keys[_key]);
	    }

        public void ClearAll()
        {
            if (m_data != null) m_data.Clear();
        }

        public void PrintAverages()
        {
            if (Logger != null) {
                foreach (string key in m_data.Keys) {
                    TimeSpan proc_time = m_data[key].average();
			        Logger.Debug("> AVERAGE [{4}]: {0,2:00}:{1,2:00}:{2,2:00}:{3,2:000} (NumberOfCalls: {5})",
				        proc_time.Hours, proc_time.Minutes, proc_time.Seconds, proc_time.Milliseconds,
				        key,
                        m_data[key].m_count
			        );
                }
            }
        }
    }
}