﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using System.Windows.Media.Imaging;

namespace HighGears.BimTables.Revit.Tools
{
    public class ResourceTool
    {
        public ResourceTool()
        {
            ResourcePrefix = Assembly.GetExecutingAssembly().GetName().Name;
        }

        string ResourcePrefix { get; set; }

        public BitmapImage GetImage(string name)
        {
            Stream s = Get(name);
            if (s == null) return null;

            BitmapImage img = new BitmapImage();
            img.BeginInit();
            img.StreamSource = s;
            img.EndInit();
            return img;
        }

        public Stream Get(string name)
        {
            string resource = Path(name);
            Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream(resource);
            return s;
        }

        string Path(string name)
        {
            name = name.Replace('\\', '.');

            return string.Format("{0}.{1}", ResourcePrefix, name);
        }

    }
}
