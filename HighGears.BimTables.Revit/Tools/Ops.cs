﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;

namespace HighGears.BimTables.Revit.Tools
{
    public static class Ops
    {
        public static void assert<T>(this object obj, bool condition) where T : Exception, new()
        {
            if (!condition) throw new T();
        }

        public static void assert<T>(this object obj, bool condition, T exception) where T : Exception
        {
            if (!condition) throw exception;
        }

        public static void presence<T>(this T obj, Action<T> func)
        {
            if (obj != null) func(obj);
        }

        static string HomeDir { get; set; }
        internal static string GetHomeDir()
        {
            if (!string.IsNullOrEmpty(HomeDir)) return HomeDir;

            string dllpath = CurrentAssemblyPath();
            return Path.GetDirectoryName(dllpath);
        }
        internal static void SetHomeDir(string homedir) { HomeDir = homedir; }
        internal static string CurrentAssemblyPath()
        {
            return Assembly.GetExecutingAssembly().Location;
        }
    }
}
