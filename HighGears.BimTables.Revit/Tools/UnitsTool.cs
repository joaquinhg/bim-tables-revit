﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighGears.BimTables.Revit.Tools
{
    public class UnitsTool
    {
        class ConversionFactors
        {
            public const double InchToFoot = 1.0 / 12.0;
            public const double InchToMilli = 25.4;
            public const double MilliToInch = 1 / InchToMilli;
            public const double MilliToFoot = MilliToInch * InchToFoot;
            public const double FeetToMeter = 0.3048;
            public const double FeetToMilli = 304.8;
            public const double FeetToInch = 12.0;
        }

        public static double InchesToFeet(double inches)
        {
            return inches * ConversionFactors.InchToFoot;
        }

        public static double FeetToInches(double feet)
        {
            return feet * ConversionFactors.FeetToInch;
        }

        public static double MilliToFeet(double milli)
        {
            return milli * ConversionFactors.MilliToFoot;
        }

        public static double FeetToMeter(double feet)
        {
            return feet * ConversionFactors.FeetToMeter;
        }

        public static double FeetToMilli(double feet)
        {
            return feet * ConversionFactors.FeetToMilli;
        }

        public static double DegToRad(double degrees)
        {
            return (degrees / 180) * Math.PI;
        }

        public static double RadToDeg(double radians)
        {
            return (radians / Math.PI) * 180;
        }
    }
}
