﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HighGears.BimTables.Revit.Tools
{
    using UI;

    public class Logger
    {
        string m_owner;
        string m_fpath;
        List<string> m_entries = new List<string>();

        public Logger(string owner, string fpath)
        {
            m_owner = owner;
            m_fpath = fpath;
        }

        public IEnumerable<string> Entries { get { return m_entries; } }

        public void Info(string fmt, params object[] args) { Add(fmt, args); }
        public void Error(string fmt, params object[] args)
        {
            Error(true, fmt, args);
        }
        void Error(bool showUser, string fmt, params object[] args)
        {
            string str = Add(fmt, args);
            if (showUser) Dialogs.Helper.Error(str);
        }
        public void Error(Exception ex)
        {
            AddException(ex, true);
        }
        public void ErrorSilent(Exception ex)
        {
            AddException(ex, false);
        }
        public void Debug(string fmt, params object[] args)
        {
#if DEBUG
            Add(fmt, args);
#endif
        }

        string Add(string fmt, params object[] args)
        {
            string str = string.Format(fmt, args);

            AddEntry(str);

            return str;
        }

        void AddEntry(string entry)
        {
            m_entries.Add(entry);
            if (!string.IsNullOrEmpty(m_fpath) && File.Exists(m_fpath))
            {
                File.AppendAllLines(m_fpath, new string[] { entry });
            }
        }

        void AddException(Exception ex, bool showUser)
        {
            string str = string.Format("{0}: {1}", ex.GetType().Name, ex.Message);
            Error(showUser, str);
        }

        public string Dump()
        {
            StringBuilder str = new StringBuilder();
            foreach (string entry in m_entries)
            {
                str.AppendLine(entry);
            }
            return str.ToString();
        }
    }
}
