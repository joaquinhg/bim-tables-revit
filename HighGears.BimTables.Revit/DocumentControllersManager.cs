﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Cad = Autodesk.Revit.DB;

namespace HighGears.BimTables.Revit
{
    using Services;
    using Services.Controllers;
    using Tools;

    public class DocumentControllersManager
    {
        public DocumentControllersManager(string basedir, Logger log)
        {
            BaseDir = basedir;
            Log = log;
            Managers = new Dictionary<Cad.Document, DocumentControllers>();
        }

        string BaseDir { get; set; }
        Logger Log { get; set; }
        Dictionary<Cad.Document, DocumentControllers> Managers;

        public DocumentControllers Get(Cad.Document _doc)
        {
            if (!Managers.ContainsKey(_doc))
            {
                DocumentControllers nman = new DocumentControllers(_doc, BaseDir);

                _doc.DocumentClosing += _doc_DocumentClosing;
                _doc.DocumentSaving += _doc_DocumentSaving;
                _doc.DocumentSavingAs += _doc_DocumentSavingAs;
                Managers[_doc] = nman;
            }
            return (Managers[_doc]);
        }

        private void _doc_DocumentSavingAs(object sender, Cad.Events.DocumentSavingAsEventArgs e)
        {
            DoDocumentSaving(e.Document);
        }

        void _doc_DocumentSaving(object sender, Cad.Events.DocumentSavingEventArgs e)
        {
            DoDocumentSaving(e.Document);
        }

        void DoDocumentSaving(Cad.Document doc)
        {
            try
            {
                if (doc != null)
                {
                    PersistManagers(doc);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        void _doc_DocumentClosing(object sender, Cad.Events.DocumentClosingEventArgs e)
        {
            try
            {
                if (e.Document != null)
                {
                    DeleteManager(e.Document);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        public void DeleteManager(Cad.Document _doc)
        {
            FinalizeManager(Get(_doc));
            Managers.Remove(_doc);
        }

        void PersistManagers(Cad.Document _doc)
        {
            Get(_doc).PersistAll();
        }

        public void RestoreManagers(Cad.Document _doc)
        {
            Get(_doc).RestoreAll();
        }

        void FinalizeManager(DocumentControllers _manager)
        {
            _manager.CleanupAll();
        }
    }
}
